package main.GUIC;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * 
 * @author mayoineko
 *
 */
public class AddOutlinePanelController extends AddFilePanelController {
	@FXML private TextArea outline;
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setControls();
		System.out.println("AddOutlinePanelController OK");
	}
	private void setControls() {
		subController.setupAddOutlinePanel(outline);
	}
}
