package main.GUIC;

public interface Subject {
	void registerObserver(IController c);
	void removeObserver(IController c);
	void notifyObservers();
}
