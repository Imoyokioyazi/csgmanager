package main.GUIC;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * 
 * @author mayoineko
 *
 */
public class AddTitlePanelController extends AddFilePanelController {
	/** FXML **/
	@FXML private TextField gamePath;
	@FXML private TextField gameTitle;
	@FXML private Button fileReference;
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setControls();
		System.out.println("AddTitlePanelController OK");
	}
	/**
	 * set Controls
	 */
	private void setControls() {
		subController.setupAddTitlePanel(gamePath,gameTitle,fileReference);
	}
}
