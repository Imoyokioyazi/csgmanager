package main.GUIC.mainPaneGUIC;

import java.io.IOException;

import main.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author mayoineko
 *
 */
public class AddFileC implements MainPanelIController{
	@FXML private MenuItem addFile;
	public static Stage subStage = new Stage();
	private static MainPanelSubject subController;
	private Parent root = null;
	//private Scene scene = null;
	
	AddFileC(MenuItem addFile,MainPanelSubject subController) {
		this.addFile = addFile;
		AddFileC.subController = subController;
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	/**
	 * menuItemが押された時のイベント処理<br>
	 * ゲームファイルの追加用ウインドウを生成し開く.
	 */
	private void setListener() {
		addFile.setOnAction(evt->{
			if(root == null)
				try {
					subStage.setTitle("ゲームファイルの追加");
					subStage.initModality(Modality.APPLICATION_MODAL);
					subStage.initOwner(Main.getStage());
					root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/addfile/AddFilePanel.fxml"));
					Scene scene = new Scene(root);
					subStage.setScene(scene);
				} catch(IOException e) {
					System.err.println(e);
				}
			subStage.show();
		});
	};
	/**
	 * reset GameList
	 * ゲームリストを更新
	 */
	public static void resetGameList() {
		gameList.resetGameList();
		subController.notifyObservers();
	}

}
