package main.GUIC.mainPaneGUIC;

import main.GameList;
import main.GUIC.IController;

/**
 * Controller
 * Interface
 * 
 * @author mayoineko
 *
 */
interface MainPanelIController extends IController {
	static GameList gameList = GameList.getInstance();
	/** 初期化メソッド*/
	@Override
	void setup();
	/** 再初期化を行う */
	@Override
	void reset();
}
