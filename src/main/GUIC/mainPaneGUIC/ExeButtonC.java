package main.GUIC.mainPaneGUIC;

import main.GameFileExecution;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * ButtonController
 * 実行ボタンを押した時の処理
 * 
 * @author mayoineko
 *
 */
class ExeButtonC implements MainPanelIController {
	@FXML private Button exeButton;
	/**
	 * Constructor
	 * 
	 * @param exeButton
	 */
	ExeButtonC(Button exeButton) {
		this.exeButton = exeButton;
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	/**
	 * Listener登録
	 */
	private void setListener() {
		exeButton.setOnMouseClicked(evt -> {
			GameFileExecution.executeGameFile(gameList.getExePath());
		});		
	}
}
