package main.GUIC.mainPaneGUIC;

import main.Main;
import main.ImageLoarder;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * CanvasController
 * 画像をCanvasに貼る処理
 * 
 * @author mayoineko
 *
 */
class CanvasC implements MainPanelIController {
	//private MainPanelSubject subController;
	@FXML private Canvas canvas;
	private GraphicsContext gc;
	/**
	 * Constructor
	 * 
	 * @param canvas
	 */
	CanvasC(Canvas canvas,MainPanelSubject subController) {
		this.canvas = canvas;
		gc = canvas.getGraphicsContext2D();
		//this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setListener();
		gc.setFont(new Font(20));
		gc.setFill(Color.RED);
		gc.fillText("Imageファイルは存在しません",
				canvas.getWidth()/3, canvas.getHeight()/2);
	}
	@Override
	public void reset() {
		String imagePath = gameList.getImagePath();
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		if(imagePath == null) {
			gc.setFill(Color.RED);
			gc.fillText("Imageファイルは存在しません",
					canvas.getWidth()/3, canvas.getHeight()/2);
		} else {
			gc.drawImage(ImageLoarder.getImages(imagePath),0,0,
					canvas.getWidth(),canvas.getHeight());
		}
	}
	/**
	 * canvasのサイズ変更
	 */
	private void setListener() {
		// widthの変更通知
		Main.getStage().widthProperty().addListener(
				new ChangeListener<Number>() {
					@Override
					public void changed(ObservableValue<? extends Number> arg0,
							Number oldValue, Number newValue) {
						double newWidth = newValue.doubleValue()-350.0;
						if(newWidth-canvas.getHeight()*4/3<=0)
							canvas.setWidth(newWidth);
						double left = ((newValue.doubleValue()-330.0)-canvas.getWidth())/2;
						if(left>0)
							AnchorPane.setLeftAnchor(canvas, left);
						else
							AnchorPane.setLeftAnchor(canvas, -left);
						System.out.print("width :"+canvas.getWidth());
						reset();
					}
				});
		// heightの変更通知
		Main.getStage().heightProperty().addListener(
				new ChangeListener<Number>() {
					@Override
					public void changed(ObservableValue<? extends Number> arg0,
							Number oldValue, Number newValue) {
						System.out.println("height:"+canvas.getHeight());
					}
				});
	}
}
