package main.GUIC.mainPaneGUIC;

import main.Genre;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

/**
 * Controller of jenreComboBox
 * @author mayoineko
 *
 */
class GenreBoxC implements MainPanelIController {
	private MainPanelSubject subController;
	@FXML private ComboBox<Genre> genreBox;
	/**
	 * ジャンルの項目
	 */
	private static ObservableList<Genre> genre_items 
		= FXCollections.observableArrayList(Genre.values());
	/**
	 * Constructor
	 * 
	 * @param genreComboBox
	 */
	GenreBoxC(ComboBox<Genre> genreBox,MainPanelSubject subController) {
		this.genreBox = genreBox;
		this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setItems();
		setSelectedListener();
	}
	@Override
	public void reset() {}
	/**
	 * Listener登録
	 */
	private void setSelectedListener() {
		genreBox.getSelectionModel().selectedItemProperty()
			.addListener(new ChangeListener<Genre>() {
				@Override
				public void changed(ObservableValue<? extends Genre> arg0,
						Genre oldValue, Genre newValue) {
					if(oldValue != newValue) {
						gameList.setGenresList(newValue);
						subController.notifyObservers();
					}
				}
			});
	}
	/**
	 * 
	 */
	private void setItems() {
		genreBox.setItems(genre_items);
	}
}
