package main.GUIC.mainPaneGUIC;

import javafx.fxml.FXML;
import javafx.scene.control.CheckMenuItem;
import main.Main;

/**
 * CheckMenuItem
 * 
 * @author mayoineko
 *
 */
public class ResizableC implements MainPanelIController {
	@FXML private CheckMenuItem resizable;
	ResizableC(CheckMenuItem resizable) {
		this.resizable = resizable;
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	/**
	 * 
	 */
	private void setListener() {
		resizable.selectedProperty().addListener(
			evt->{
				boolean isResiza = resizable.isSelected();
				Main.getStage().setResizable(!isResiza);
		});
	}
}
