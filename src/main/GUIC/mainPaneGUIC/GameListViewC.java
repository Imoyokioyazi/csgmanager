package main.GUIC.mainPaneGUIC;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 * Controller of gameListView
 * 
 * @author mayoineko
 *
 */
class GameListViewC implements MainPanelIController {
	private MainPanelSubject subController;
	@FXML private ListView<String> gameListView;
	/** Items of ListView */
	private static ObservableList<String> items = FXCollections
			.observableArrayList();
	/**
	 * Constructor
	 * 
	 * @param gameListView
	 */
	GameListViewC(ListView<String> gameListView,MainPanelSubject subController) {
		this.gameListView = gameListView;
		this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		// Listener登録
		setListener();
		// item登録
		gameListView.setItems(items);
		items.addAll(gameList.getTitles());
	}
	@Override
	public void reset() {
		if(items.equals(gameList.getGameList())) return;
		items.clear();
		gameListView.getItems().clear();
		items.addAll(gameList.getGameList());
	}
	/**
	 * Listener登録
	 */
	private void setListener() {
		gameListView.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> arg0,
							String oldValue, String newValue) {
						gameList.getIndex(newValue);
						subController.notifyObservers();
					}
				});
	}
}
