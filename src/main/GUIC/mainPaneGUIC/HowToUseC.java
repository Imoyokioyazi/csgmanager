package main.GUIC.mainPaneGUIC;

import main.Main;
import main.Setting;
import main.TextReader;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * HowToUseMenuItem Controller
 * 
 * @author mayoineko
 *
 */
class HowToUseC implements MainPanelIController {
	@FXML private MenuItem howToUse;
	HowToUseC(MenuItem howToUse) {
		this.howToUse = howToUse;
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	/**
	 * Listenerの登録
	 * 項目にアクションが起こった時、別画面のウインドウを新規で作成し、<br>
	 * ファイルを読み込んでテキストをそこへ表示する。
	 */
	private void setListener() {
		howToUse.setOnAction(evt->{
			Stage readme = new Stage();
			readme.setTitle("readme");
			readme.initModality(Modality.APPLICATION_MODAL);
			readme.initOwner(Main.getStage());
			ScrollPane textPane = new ScrollPane();
			Text text = new Text();
			text.setText(TextReader.reads(Setting.SETTING_FOLDER_PATH+"readme.txt"));
			textPane.setContent(text);
			text.setWrappingWidth(600);
			Scene scene = new Scene(textPane);
            readme.setScene(scene);
            // 新しいウインドウを表示
            readme.show();
		});
	}
}
