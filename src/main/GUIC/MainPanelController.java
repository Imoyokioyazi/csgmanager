package main.GUIC;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import main.GUIC.mainPaneGUIC.*;

/**
 * MainPanel Controller
 * MainPanelのコントローラクラス
 * 実際の処理はSubControllerで定義
 * @author mayoineko
 *
 */
public class MainPanelController implements Initializable {
	/** FXML */
	@FXML private CheckMenuItem resizable;
	@FXML private MenuItem howToUse;
	@FXML private MenuItem addFile;
	@FXML private HBox mainPane;
	/** SubController **/
	protected MainPanelSubController subController = MainPanelSubController.getInstance();
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setControls();
		setMainPane();
		System.out.println("MainPanelController OK");
	}
	/**
	 * setCheckMenuItemController
	 */
	private void setControls() {
		subController.setupMainPanel(howToUse,addFile,resizable);
	}
	/**
	 * Load OperatePanel.fxml and DetailPanel.fxml
	 * 
	 */
	private void setMainPane() {
		//operationPanel追加
		try {
			VBox operatePane = (VBox)FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main/OperatePanel.fxml"));
			mainPane.getChildren().add(operatePane);
		} catch(IOException e) {
			System.err.println("Can't be read 'OperatePanel.fxml'.");
		}
		//detailPanel追加
		try {
			VBox detailPane = (VBox)FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main/DetailPanel.fxml"));
			mainPane.getChildren().add(detailPane);
			HBox.setHgrow(detailPane, Priority.ALWAYS);
		} catch(IOException e) {
			System.err.println("Can't be read 'DetailPanel.fxml'.");
		}
	}
	
}