package main.GUIC;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * 
 * @author mayoineko
 *
 */
public class AddImagePanelController extends AddFilePanelController {
	/** FXML **/
	@FXML private TextField imagePath;
	@FXML private Button fileReference;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setControls();
		System.out.println("AddImagePanelController OK");
	}
	/**
	 * 
	 */
	private void setControls() {
		subController.setupAddImagePanel(imagePath,fileReference);
	}
}
