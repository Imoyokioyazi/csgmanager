package main.GUIC;

import java.net.URL;
import java.util.ResourceBundle;

import main.Genre;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;

/**
 * OperatePanelController
 * 
 * @author mayoineko
 *
 */
public class OperatePanelController extends MainPanelController implements Initializable {
	/** FXML **/
	@FXML private ComboBox<Genre> genreBox;
	@FXML private ListView<String> gameListView;
	@FXML private Button exeButton;
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setControls();
		System.out.println("OperatePanelController OK");
	}
	private void setControls() {
		subController.setupOperatePanel
				(genreBox,gameListView,exeButton);
	}
}
