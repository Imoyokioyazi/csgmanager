package main.GUIC;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import main.GUIC.addgamePaneGUIC.AddFilePanelSubController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * 
 * @author mayoineko
 *
 */
public class AddFilePanelController implements Initializable {
	protected AddFilePanelSubController subController = AddFilePanelSubController.getInstance();
	/** FXML **/
	@FXML private Button okButton;
	@FXML private Button cancelButton;
	@FXML private VBox titlePane;
	@FXML private HBox genrePane;
	@FXML private VBox imagePane;
	@FXML private AnchorPane outlinePane;
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setControls();
		setAddTitlePanel();
		setAddGenrePanel();
		setAddImagePanel();
		setAddOutlinePanel();
		System.out.println("AddFilePanelController OK");
	}
	private void setControls() {
		subController.setupAddFilePanel(okButton,cancelButton);
	}
	/**
	 * Load AddTitlePanel.fxml<br>
	 * AddTitle.fxmlを読み込む
	 */
	public void setAddTitlePanel() {
		try {
			VBox titlePane = (VBox)FXMLLoader.load(getClass().getClassLoader().getResource("fxml/addfile/AddTitlePanel.fxml"));
			this.titlePane.getChildren().add(titlePane);
		} catch(IOException e) { 
			System.err.println("Can't be read 'AddTitlePanel.fxml'.");
		}
	}
	/**
	 * Load AddGenrePanel.fxml<br>
	 * AddGenre.fxmlを読み込む
	 */
	public void setAddGenrePanel() {
		try {
			HBox genrePane = (HBox)FXMLLoader.load(getClass().getClassLoader().getResource("fxml/addfile/AddGenrePanel.fxml"));
			this.genrePane.getChildren().add(genrePane);
		} catch(IOException e) {
			System.err.println("Can't be read 'AddGenrePanel.fxml'.");
		}
	}
	/**
	 * Load AddImagePanel.fxml<br>
	 * AddImage.fxmlを読み込む
	 */
	public void setAddImagePanel() {
		try {
			VBox imagePane = (VBox)FXMLLoader.load(getClass().getClassLoader().getResource("fxml/addfile/AddImagePanel.fxml"));
			this.imagePane.getChildren().add(imagePane);
		} catch(IOException e) { 
			System.err.println("Can't be read 'AddImagePanel.fxml'.");
		}
	}
	/**
	 * Load AddOutlinePanel.fxml<br>
	 * AddOutline.fxmlを読み込む
	 */
	public void setAddOutlinePanel() {
		try {
			AnchorPane outlinePane = (AnchorPane)FXMLLoader.load(getClass().getClassLoader().getResource("fxml/addfile/AddOutlinePanel.fxml"));
			this.outlinePane.getChildren().add(outlinePane);
		} catch(IOException e) { 
			System.err.println("Can't be read 'AddOutlinePanel.fxml'.");
		}
	}
}
