package main.GUIC;

public interface IController {
	/** 初期化メソッド*/
	void setup();
	/** 再初期化を行う */
	void reset();
}
