package main.GUIC;


import java.net.URL;
import java.util.ResourceBundle;

import main.Genre;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

/**
 * 
 * @author mayoineko
 *
 */
public class AddGenrePanelController extends AddFilePanelController{
	/** FXML **/
	@FXML private ComboBox<Genre> genreBox1;
	@FXML private ComboBox<Genre> genreBox2;
	@FXML private ComboBox<Genre> genreBox3;
	@Override
	public void initialize(URL url,ResourceBundle rb) {
		setControls();
		System.out.println("AddGenrePanelController OK");
	}
	/**
	 * 
	 */
	private void setControls() {
		subController.setupAddGenrePanel(genreBox1,genreBox2,genreBox3);
	}
}
