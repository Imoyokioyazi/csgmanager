package main.GUIC.addgamePaneGUIC;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import main.Genre;

/**
 * GenreBox Controller
 * 
 * @author mayoineko
 *
 */
class GenreBoxC implements AddFilePanelIController{
	@FXML private ComboBox<Genre>[] genreBoxs;
	private ObservableList<Genre> genreItems = FXCollections.observableArrayList();
	private Genre[] genres;
	GenreBoxC(AddFilePanelSubject subController,ComboBox<Genre>... genreBoxs) {
		this.genreBoxs = genreBoxs;
		this.genres = new Genre[genreBoxs.length];
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setItems();
	}
	@Override
	public void reset() {
		register.setGenres(genres);
		for(int i=0;i<genres.length;i++)
			genres[i] = genreBoxs[i].getSelectionModel().getSelectedItem();
	}
	private void setItems() {
		for(Genre genre:Genre.values())
			if(genre != Genre.ALL)
				genreItems.add(genre);
		for(ComboBox<Genre> genreBox: genreBoxs)
			genreBox.setItems(genreItems);
	}
}
