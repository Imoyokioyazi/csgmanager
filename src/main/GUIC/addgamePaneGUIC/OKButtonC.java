package main.GUIC.addgamePaneGUIC;

import main.GameList;
import main.GUIC.mainPaneGUIC.AddFileC;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;import javafx.scene.control.ButtonType;

/**
 * OKButton Controller
 * 
 * @author mayoineko
 *
 */
class OKButtonC implements AddFilePanelIController {
	private AddFilePanelSubject subController;
	@FXML private Button okButton;
	OKButtonC(AddFilePanelSubject subController,Button okButton) {
		this.okButton = okButton;
		this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	/**
	 * listenerの登録
	 */
	private void setListener() {
		okButton.setOnAction(evt-> {
			subController.notifyObservers();
			boolean[] error = register.setRegister();
			for(int i=0;i<5;i++) {
				if(error[i]) {
					Alert alert 
					= new Alert(AlertType.CONFIRMATION,"",ButtonType.OK);
					alert.setTitle("以下の登録情報が不足しています");
					String alertText = "";
					if(i == 0) {
						alertText = "ゲームフォルダのパス";
					} else if(i == 1) {
						alertText = "ゲームのタイトル";
					} else if(i == 2) {
						alertText = "一つ以上のジャンルの選択";
					} else if(i == 3) {
						alertText = "イメージファイルのパス";
					} else {
						alertText = "概要のテキスト";
					}
					alert.setHeaderText(alertText);
					alert.showAndWait().ifPresent(response -> {
						if(response == ButtonType.OK);
					});;
				}
			}
			if(!error[0] && (error[1] || error[2] || error[3] || error[4])) {
				Alert confirm 
					= new Alert(AlertType.CONFIRMATION,"",ButtonType.OK,ButtonType.CANCEL);
				confirm.setHeaderText("情報が抜けていますが登録してもよろしいですか？");
				confirm.showAndWait().ifPresent(response -> {
					if(response == ButtonType.OK) {
						register.addGameFolder();
						AddFileC.resetGameList();
						AddFileC.subStage.close();
						
					} else {
						
					}
				});;
			} else if(!error[0] && !error[1] && !error[2] && !error[3] && !error[4]){
				Alert confirm 
				= new Alert(AlertType.CONFIRMATION,"",ButtonType.OK,ButtonType.CANCEL);
			confirm.setHeaderText("登録してもよろしいですか？");
			confirm.showAndWait().ifPresent(response -> {
				if(response == ButtonType.OK) {
					register.addGameFolder();
					AddFileC.resetGameList();
					AddFileC.subStage.close();
				} else {
					
				}
			});;
			}
		});
	}
}
