package main.GUIC.addgamePaneGUIC;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * GameTitleC
 * 
 * @author mayoineko
 *
 */
class GameTitleC implements AddFilePanelIController {
	/** FXML **/
	@FXML private TextField gameTitle;
	GameTitleC(AddFilePanelSubject subController,TextField gameTitle) {
		this.gameTitle = gameTitle;
		subController.registerObserver(this);
	}
	@Override
	public void setup() { }
	@Override
	public void reset() {
		// タイトルテキストボックスから取得
		String title = gameTitle.getText();
		if(title.equals(""))
			setTitle();
		title = gameTitle.getText();
		register.setGameTitle(title);
	}
	private void setTitle() {
		String title = register.getGamePath();
		String gameTitle ="";
		int i = title.lastIndexOf("/");
		if(i==-1)
			i = title.lastIndexOf("¥");
		if(i==-1)
			i = title.lastIndexOf("￥");
		for(int j=i+1;j<title.length();j++) {
			if(title.charAt(j) == '.') break;
			gameTitle += title.charAt(j);
		}
		this.gameTitle.setText(gameTitle);
	}
}
