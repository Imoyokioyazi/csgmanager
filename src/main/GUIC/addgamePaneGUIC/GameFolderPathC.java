package main.GUIC.addgamePaneGUIC;

import java.util.List;
import java.io.File;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

/**
 * TextField Controller
 * テキストフィールドを定義する
 * 
 * @author mayoineko
 *
 */
class GameFolderPathC implements AddFilePanelIController {
	/** FXML **/
	@FXML private TextField filePath;
	private AddFilePanelSubject subController;
	private String path;
	GameFolderPathC(AddFilePanelSubject subController,TextField filePath) {
		this.filePath = filePath;
		this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {
		if(register.getGamePath()!=null)
			filePath.setText(register.getGamePath());
	}
	/**
	 * 
	 */
	private void setListener() {
		filePath.setOnDragOver((evt)->{
			Dragboard db = evt.getDragboard();
			if (db.hasFiles())
				evt.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			evt.consume();
		});
		filePath.setOnDragDropped((evt)->{
			boolean success = false;
			// ファイルの場合だけ受け付ける
			Dragboard db = evt.getDragboard();
			if (db.hasFiles()) {
				List<File> list = db.getFiles();
				for(File file: list) {
					path = file.getAbsolutePath();
					filePath.setText(path);
				}
				success = true;
			}
			register.setGamePath(path);
			subController.notifyObservers();
			evt.setDropCompleted(success);
			evt.consume();
		});
	}
}
