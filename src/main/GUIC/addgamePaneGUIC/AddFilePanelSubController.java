package main.GUIC.addgamePaneGUIC;

import java.util.ArrayList;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import main.Genre;
import main.GUIC.IController;

/**
 * AddFilePanel SubController
 * ここで各Control(Button,ListView,etc...)用コントローラクラスを呼び出し実行する
 * @author mayoineko
 *
 */
public class AddFilePanelSubController implements AddFilePanelSubject{
	private static AddFilePanelSubject subController = new AddFilePanelSubController();
	private ArrayList<IController> observers;
	//Controllers
	private OKButtonC okButtonC;
	private CancelButtonC cancelButtonC;
	
	private GameFolderPathC gamePathC;
	private GameTitleC gameTitleC;
	private GameReferenceC gameReferenceC;
	
	private GenreBoxC genreBoxC;
	
	private ImageFilePathC imagePathC;
	private ImageReferenceC imageReferenceC;
	
	private OutlineC outlineC;
	/**
	 * initialize observer
	 * observer初期化
	 */
	private AddFilePanelSubController() {
		observers = new ArrayList<IController>();
	}
	/**
	 * Instance of this class
	 * 唯一のインスタンス
	 * @return
	 */
	public static AddFilePanelSubController getInstance() {
		return (AddFilePanelSubController)subController;
	}
	/**
	 * set Controls of AddFilePanel.fxml
	 * AddFilePanel.fxmlの部品を定義する
	 * @param controls
	 */
	public void setupAddFilePanel(Object...controls) {
		okButtonC = new OKButtonC(this,(Button)controls[0]);
		okButtonC.setup();
		cancelButtonC = new CancelButtonC((Button)controls[1]);
		cancelButtonC.setup();
	}
	/**
	 * set Controls of AddTitlePanel.fxml
	 * AddTitlePanel.fxmlの部品を定義する
	 * @param controls
	 */
	public void setupAddTitlePanel(Object...controls) {
		gamePathC = new GameFolderPathC(this,(TextField)controls[0]);
		gamePathC.setup();
		gameTitleC = new GameTitleC(this,(TextField)controls[1]);
		gameTitleC.setup();
		gameReferenceC = new GameReferenceC(this,(Button)controls[2]);
		gameReferenceC.setup();
	}
	/**
	 * set Controls of AddGenrePanel.fxml 
	 * AddGenrePanel.fxmlの部品を定義する
	 * @param controls
	 */
	public void setupAddGenrePanel(Object...controls) {
		genreBoxC = new GenreBoxC(this,(ComboBox<Genre>)controls[0],
				(ComboBox<Genre>)controls[1],(ComboBox<Genre>)controls[2]);
		genreBoxC.setup();
	}
	/**
	 * set Controls of AddImagePanel.fxml
	 * AddImagePanel.fxmlの部品を定義する
	 * @param controls
	 */
	public void setupAddImagePanel(Object...controls) {
		imagePathC = new ImageFilePathC(this,(TextField)controls[0]);
		imagePathC.setup();
		imageReferenceC = new ImageReferenceC(this,(Button)controls[1]);
		imageReferenceC.setup();
	}
	/**
	 * set Controls of AddOutlinePanel.fxml
	 * AddOutlinePanel.fxmlの部品を定義する
	 * @param controls
	 */
	public void setupAddOutlinePanel(Object...controls) {
		outlineC = new OutlineC(this,(TextArea)controls[0]);
		outlineC.setup();
	}
	@Override
	public void registerObserver(IController c) {
		observers.add(c);
	}
	@Override
	public void notifyObservers() {
		for(int i=0;i<observers.size();i++) {
			AddFilePanelIController observer = (AddFilePanelIController)observers.get(i);
			observer.reset();
		}
	}
	@Override
	public void removeObserver(IController c) {
		int i= observers.indexOf(c);
		if (i>=0)
			observers.remove(i);
	}
}
