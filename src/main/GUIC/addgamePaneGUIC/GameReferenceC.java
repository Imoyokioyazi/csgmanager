package main.GUIC.addgamePaneGUIC;

import java.io.File;

import main.GUIC.mainPaneGUIC.AddFileC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.DirectoryChooser;



/**
 * 現在使用不可
 * ファイルの参照を行うクラス
 *
 * @author mayoineko
 *
 */
class GameReferenceC implements AddFilePanelIController{
	/** FXML **/
	@FXML private Button fileReference;
	private AddFilePanelSubject subController;
	GameReferenceC(AddFilePanelSubject subController,Button fileReference) {
		this.fileReference = fileReference;
		this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	private void setListener() {
		fileReference.setOnAction(evt-> {
			DirectoryChooser dc = new DirectoryChooser();
			dc.setTitle("ゲームフォルダ選択");
			File importFile = dc.showDialog(AddFileC.subStage);
            if (importFile != null) {
            	String path = importFile.toString();
            	register.setGamePath(path);
            	subController.notifyObservers();
            }
		});
	}
}
