package main.GUIC.addgamePaneGUIC;

import java.io.File;

import main.GUIC.mainPaneGUIC.AddFileC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;import javafx.stage.FileChooser;


/**
 * 現在使用不可
 * ファイルの参照を行うクラス
 *
 * @author mayoineko
 *
 */
class ImageReferenceC implements AddFilePanelIController{
	/** FXML **/
	@FXML private Button fileReference;
	private AddFilePanelSubject subController;
	ImageReferenceC(AddFilePanelSubject subController,Button fileReference) {
		this.fileReference = fileReference;
		this.subController = subController;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	private void setListener() {
		fileReference.setOnAction(evt-> {
			FileChooser fc = new FileChooser();
			fc.setTitle("ゲームフォルダ選択");
			File importFile = fc.showOpenDialog(AddFileC.subStage);

            if (importFile != null) {
            	register.setImagePath(importFile.toString());
            	subController.notifyObservers();
            }
		});
	}
}
