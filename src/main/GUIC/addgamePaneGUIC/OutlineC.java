package main.GUIC.addgamePaneGUIC;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * OutlineC
 * 
 * @author mayoineko
 *
 */
class OutlineC implements AddFilePanelIController {
	/** FXML **/
	@FXML private TextArea outline;
	OutlineC(AddFilePanelSubject subController,TextArea outline) {
		this.outline = outline;
		subController.registerObserver(this);
	}
	@Override
	public void setup() {}
	@Override
	public void reset() {
		String text = outline.getText();
		register.setOutline(text);
	}
}
