package main.GUIC.addgamePaneGUIC;

import main.GUIC.mainPaneGUIC.AddFileC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * CancelButton
 * ファイル追加用ウインドウを閉じる
 * 
 * @author mayoineko
 *
 */
class CancelButtonC implements AddFilePanelIController {
	@FXML private Button cancelButton;
	CancelButtonC(Button cancelButton) {
		this.cancelButton = cancelButton;
	}
	@Override
	public void setup() {
		setListener();
	}
	@Override
	public void reset() {}
	private void setListener() {
		cancelButton.setOnAction(evt-> {
			AddFileC.subStage.close();
		});
	}
}
