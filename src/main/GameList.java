package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * GameFolder内のFolder
 * 保存しているクラス
 * 
 * @author mayoineko
 *
 */
public class GameList {
	/** 現在選択されているゲームの番号 */
	private static int currentIndex;
	/**
	 * GameFolderの中身をリスト化<br>
	 * ゲームタイトルのリスト
	 */
	private static String[] fileList;
	/** GameFile */
	private static Game[] gameList;
	/** ジャンルを指定 */
	private ObservableList<String> genreGames
		= FXCollections.observableArrayList();
	/** GameListクラスのインスタンス */
	private static GameList instance = new GameList();
	/**
	 * GameFolderに存在するフォルダ全て読み込み<br>
	 * List化して登録する
	 */
	private GameList() {
		setGameList();
	}
	/**
	 * create GameList
	 * ゲームリストの作成
	 */
	private void setGameList() {
		setFileList();
		int size = 0;
		if(fileList != null) {
			size= fileList.length;
			gameList = new Game[size];
			for(int i=0;i<size;i++) {
				String title = fileList[i];
				String path  = Setting.GAMEFOLDER_PATH+fileList[i]+"/";
				gameList[i] = new Game(path,title);
			}
			// 初期設定
			setGenresList(Genre.ALL);
		}else {
			System.err.println("フォルダの読み込み失敗\n原因としてカレントディレクトリ、フォルダ名が違うことが挙げられます");
			System.exit(0);
		}
	}
	/**
	 * ゲームファイルの一覧を読み込む
	 */
	private void setFileList() {
		try {
			fileList = FileListLoader.getFileList(Setting.GAMEFOLDER_PATH);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * GameListのインスタンス
	 * 
	 * @return GameList
	 */
	public static GameList getInstance() {
		return instance;
	}
	/**
	 * ゲームのタイトルをリストにして返す
	 * @return
	 */
	public String[] getTitles() {
		return fileList;
	}
	/**
	 * 引数から索引番号を返す<br>
	 * なければ0を返す
	 * @param item
	 * @return
	 */
	public int getIndex(String item) {
		for(int i=0;i<gameList.length;i++)
			if(fileList[i].equals(item))
				return currentIndex = i;
		return currentIndex = 0;
	}
	/**
	 * genreを指定して<br>
	 * gameListを作成しリスト化する
	 * 
	 * @param genre
	 * @return ObservableList
	 */
	public void setGenresList(Genre genre) {
		genreGames.clear();
		if(genre == Genre.ALL) { genreGames.addAll(fileList); return; }
		for(int i=0;i<gameList.length;i++)
			if(String.valueOf(gameList[i].getGenre())
					.indexOf(String.valueOf(genre.getLabel())) >= 0)
				genreGames.add(gameList[i].getTitle());
	}
	/**
	 * 現在選択されている索引番号
	 * 
	 * @return
	 */
	public int getCurrentIndex() {
		return currentIndex;
	}
	/** 
	 * 現在選択されている索引番号から<br>
	 * Genreを取得
	 * 
	 * @return
	 */
	/*
	public String getGenre() {
		return gameList[currentIndex].getGenre();
	}
	*/
	public String getGenre() {
		return String.valueOf(gameList[currentIndex].getGenre());
	}
	/**
	 * 現在選択されている索引番号から<br>
	 * TextPathを取得
	 * @return
	 */
	public String getTextPah() {
		return gameList[currentIndex].getTextPath();
	}
	/**
	 * 現在選択されている索引番号から<br>
	 * ImagePathを取得
	 * @return
	 */
	public String getImagePath() {
		return gameList[currentIndex].getImagePath();
	}
	/**
	 * 現在選択されている索引番号から<br>
	 * ExePathを取得
	 * @return
	 */
	public String getExePath() {
		return gameList[currentIndex].getExePath();
	}
	/**
	 * genreGamesを取得
	 * 
	 * @return
	 */
	public ObservableList<String> getGameList() {
		return genreGames;
	}
	public final void resetGameList() {
		setGameList();
	}
}
