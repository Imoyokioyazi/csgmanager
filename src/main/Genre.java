package main;

/**
 * Genre
 * 
 * @author mayoineko
 *
 */
public enum Genre {
	/*
	ALL("ALL"),
	シューティング("#STG"),
	アクション("#ACT"),
	アドベンチャー("#ADV"),
	シミュレーション("#SML"),
	ロールプレイング("#RPG"),
	パズル("#PZL"),
	その他("#OTH");
	*/
	ALL(0),
	シューティング(1),
	アクション(2),
	アドベンチャー(3),
	シミュレーション(4),
	ロールプレイング(5),
	パズル(6),
	テーブルゲーム(7),
	その他(8);
	/*
	private String label;
	Genre(String label) { this.label = label; }
	public String getLabel() { return label; }
	*/
	private int label;
	Genre(int label) { this.label = label; }
	public int getLabel() { return label; }
}
