package main;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * 設定ファイルの読み込み
 * @author mayoineko
 *
 */
public class Configuration {
	/**
	 * 単一の制御テーブル
	 */
	private static Configuration configuration = new Configuration();
	/**
	 * getter
	 * @return
	 */
	public static Configuration getConfiguration() {
		return configuration;
	}
	/**
	 * 設定ファイル名
	 */
	private static final String CONFIG_FILE = "setting.conf";
	/**
	 * プロパティ
	 */
	private Properties config = new Properties();
	private Configuration() {
		load();
	}
	/**
	 * ファイルの読み込み
	 */
	public void load() {
		try {
			config.load(Files.newBufferedReader(Paths.get(CONFIG_FILE),StandardCharsets.UTF_8));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public String getStringProperty(String key) {
		return config.getProperty(key);
	}
	public String getStringProperty(String key, String defaultValue) {
		return config.getProperty(key,defaultValue);
	}
	/**
	 * 設定項目に対応する数値を得る
	 * 
	 * @param key キー
	 * @return 整数
	 */
	public int getIntProperty(String key) {
		return Integer.parseInt(config.getProperty(key));
	}

	/**
	 * 設定項目に対応する数値を得る(数値がなければ暗黙値を返す)
	 * 
	 * @param key キー
	 * @param defaultValue 暗黙値
	 * @return 整数
	 */
	public int getIntProperty(String key, String defaultValue) {
		return Integer.parseInt(config.getProperty(key, defaultValue));
	}

	/**
	 * 設定項目に対応する二進値を得る
	 * 
	 * @param key キー
	 * @return 真偽
	 */
	public boolean getBooleanProperty(String key) {
		return config.getProperty(key).equals("true") ? true : false;
	}
	/**
	 * 設定項目に対応する二進値を得る(二進値がなければ暗黙値を返す)
	 * 
	 * @param key キー
	 * @param defaultValue 暗黙値
	 * @return 真偽
	 */
	public boolean getBooleanProperty(String key, String defaultValue) {
		return config.getProperty(key, defaultValue).equals("true") ? true
				: false;
	}
	/**
	 * configurationの設定ファイル読み込みテスト
	 * @param args
	 */
	public static void main(String[] args) {
		Configuration conf = new Configuration();
		System.out.println(conf.getStringProperty("Title"));
		System.out.println(conf.getStringProperty("Drive"));
		System.out.println(conf.getStringProperty("GameFolder_Path"));
		
		//System.out.printf("Confiruration Test Deck.deckNumbers = %d", conf.getIntProperty("Deck.deckNumbers"));
	}
}
