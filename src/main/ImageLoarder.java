package main;

import javafx.scene.image.Image;

/**
 * 画像を読み込む
 * 
 * @author mayoineko
 *
 */
public class ImageLoarder {
	/** counter */
	private static int count = 0;
	/** maximize counter*/
	private final static int MAX_CONT = 26;
	/** 画像のキャッシュ */
	private static Image[] images = new Image[26];
	/** パスのキャッシュ */
	private static String[] paths = new String[26];
	/**
	 * パス指定で画像の取得
	 * 
	 * @param path
	 * @return
	 */
	public static Image getImages(String path) {
		String filePath = "file:" + path;
		System.out.println(filePath);
		if(count == MAX_CONT)
			deleteCache();
		for(int i=0;i<count+1;i++) {
			if(paths[i] == null) break;
			if(paths[i].equals(filePath)) return images[i];
		}
		paths[count] = filePath;
		images[count] = new Image(filePath);
		return images[count++];
	}
	/**
	 * delete cache
	 */
	private static void deleteCache() {
		images = new Image[MAX_CONT];
		paths = new String[MAX_CONT];
		count = 0;
	}
}
