package main;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.Parent;
import javafx.stage.Stage;
/**
 * Main of Application
 * 実行クラス
 * 
 * @author mayoineko
 *
 */
public class Main extends Application{
	private static Stage stage = null;
	/** FXMLファイル用 */
	private Parent root;
	/** 画面 */
	private Scene scene;
	@Override
	public void start(Stage stage) throws Exception {
		Main.stage = stage;
		setFXMLFile();
		setWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	} 
	/**
	 * FXMLファイルを読み込む
	 */
	private void setFXMLFile() {
		try {
			// Structure of GUI
			root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main/MainPanel.fxml"));
			System.out.println(getClass().getClassLoader().getResource("fxml/main/MainPanel.fxml").toString());
		} catch(IOException e) {
			System.err.println("Can't be read 'MainPanel.fxml'.");
			System.exit(0);
		}
	}
	/**
	 * ウインドウ設定
	 */
	private void setWindow() {
		stage.setTitle(Setting.TITLE);// set Title
		stage.setMinWidth(Setting.MIN_WIDTH);
		stage.setMinHeight(Setting.MIN_HEIGHT);
		stage.setWidth(Setting.MIN_WIDTH);
		stage.setHeight(Setting.MIN_HEIGHT);
		stage.setResizable(Setting.isResiza);// not resized
	}
	/**
	 * getter of stage
	 * @return
	 */
	public static Stage getStage() {
		return stage;
	}
	/**
	 * 実行メソッド
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
