package main;

import java.io.File;
import java.io.IOException;

/**
 * ExeCute GameFile
 * 拡張子がjarとexeのもののみに対応
 * 
 * @author mayoineko
 *
 */
public class GameFileExecution {
	private static String CURRENT_DISK = Setting.Drive;
	/**
	 * ゲームファイルを実行する
	 * 引数で得たパスを作業ディレクトリ用パスと<br>
	 * ゲームファイルパスに分ける
	 * 
	 * @param path
	 *            　
	 */
	public static void executeGameFile(String gameFile) {
		String directory = "";
		String game = ".";
		int i=0;
		for(;i<gameFile.lastIndexOf("/");i++) {
			directory += String.valueOf(gameFile.charAt(i));
		}
		for(;i<gameFile.length();i++) {
			game += String.valueOf(gameFile.charAt(i));
		}
		executeFile(directory,game);
	}
	/**
	 * ゲームファイルを実行する<br>
	 * 拡張子ごとに処理
	 * 
	 * もし将来的に変更があるならここは気をつけるべき
	 * @param directory
	 * @param file
	 */
	private static void executeFile(final String directory,final String file) {
		if (isJarFile(file))// 「jar」
			try {
				ProcessBuilder pb = new ProcessBuilder("java","-jar",file);
				pb.directory(new File(directory));
				Process process = pb.start();
				try {
					int ret = process.waitFor();
					System.out.println("返り値"+ret);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				System.err.println("Can't be read a game file.");
				e.printStackTrace();
			}
		else // windows用実行ファイル「exe」
			try {
				String newFile="";
				for(int i=2;i<file.length();i++) {
					newFile+=file.charAt(i);
				}
				ProcessBuilder pb = new ProcessBuilder("cmd","/c",newFile);
				pb.directory(new File(CURRENT_DISK+directory));
				System.out.println(pb.directory().getPath());
				System.out.println(directory+"\n"+newFile);
				Process process = pb.start();
				try {
					int ret = process.waitFor();
					System.out.println("返り値"+ret);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	/**
	 * 拡張子jarならtrue<br>
	 * exeならfalse
	 * 
	 * @param path
	 * @return boolean
	 */
	private static boolean isJarFile(String path) {
		if (path.lastIndexOf("jar") >= 0)
			return true;
		else return false;
	}
}
