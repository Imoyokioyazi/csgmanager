package main;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * fileを読みこみ
 * listにするクラス
 * 
 * @author mayoineko
 *
 */
public class FileListLoader {
	/** maximize counter*/
	private final static int MAX_COUNT = 100;
	/** counter */
	private static int count;
	/** systemCounter */
	private static int sCount;
	/** folderPaths */
	private static String[] folderPaths = new String[MAX_COUNT];
	/** systemPaths */
	private static String[] systemPaths = new String[MAX_COUNT];
	/** fileLists */
	private static String[][] lists = new String[MAX_COUNT][];
	private static String[][] systemLists = new String[MAX_COUNT][];
	/**
	 * Folderを指定してfileを配列で返す
	 * 
	 * @param filePath
	 */
	public static String[] getFileList(String folderPath) throws IOException{
		// Cache delete
		if (count == MAX_COUNT)
			resetCache();
		// 同名のパスがあればそこから中身のリストを返す
		for (int i = 0; i < MAX_COUNT; i++)
			if (folderPaths.equals(folderPath))
				return lists[i];
		// なければ新たにパスを追加し、中身をリストにして追加する
		folderPaths[count] = folderPath;
		lists[count] = new File(folderPath).list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.indexOf(".") == 0 || name.indexOf("#") == 0)
					return false;
				return true;
			}
		});
		return lists[count++];
	}
	/**
	 * ゲームに使用されるファイルとは関係のない
	 * 説明用のファイルのみをリスト化する
	 * 
	 * @param folderPath
	 * @return
	 */
	public static String[] getSystemFileList(String folderPath) {
		// Cache delete
		if (sCount == MAX_COUNT)
			resetCache();
		// 同名のパスがあればそこから中身のリストを返す
		for (int i = 0; i < MAX_COUNT; i++)
			if (systemPaths.equals(folderPath))
				return lists[i];
		// なければ新たにパスを追加し、中身をリストにして追加する
		systemPaths[sCount] = folderPath;
		systemLists[sCount] = new File(folderPath).list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.indexOf("$") == 0)
					return true;
				return false;
			}
		});
		return systemLists[sCount++];
	}
	/**
	 * delete cache
	 */
	private static void resetCache() {
		folderPaths = new String[MAX_COUNT];
		lists = new String[MAX_COUNT][];
		systemPaths = new String[MAX_COUNT];
		systemLists = new String[MAX_COUNT][];
		sCount = 0;
		count = 0;
	}
	/**
	 * fileListから拡張子と同じfileを返す<br>
	 * なければ""を返す
	 * 
	 * @param fileList
	 * @param extension
	 * @return
	 */
	public static String searchFile(String[] fileList,String... extension) {
		try{
			for (String file : fileList)
				for (String e : extension)
					if (file.lastIndexOf("." + e) >= 0)
						return file;
			System.err.println("The file do't exist.");
			return null;
		}catch(Exception e) {
			System.err.println("fileListが存在しません");
			return null;
		}
	}
	/**
	 * テスト用
	 * @param args
	 */
	public static void main(String[] args) {
		String[] fileList = null;
		try {
			fileList = FileListLoader.getFileList(Setting.GAMEFOLDER_PATH);
		} catch(IOException e) {
			e.printStackTrace();
		}
		for(String file: fileList){
			System.out.println(file);
			String[] gameList = FileListLoader.getSystemFileList(Setting.GAMEFOLDER_PATH+file+"/");
			for(String game: gameList) {
				System.out.println(game);
			}
		}
	}
}
