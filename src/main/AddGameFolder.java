package main;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * AddGameFolder
 * 
 * @author mayoineko
 *
 */
public class AddGameFolder {
	/** 追加するゲームの情報を保存しているクラス **/
	private static Register register;

	/**
	 * gameFolderをコピー<br>
	 * タイトル変更<br>
	 * 説明用ファイル生成<br>
	 * イメージファイルコピー<br>
	 * ジャンルファイル生成<br>
	 * 
	 * @param register
	 */
	public final static void addGame(Register register) {
		AddGameFolder.register = register;
		copyfolder();
		outputText();
	}

	/**
	 * 
	 */
	private static void copyfolder() {
		String newFolderPath = Setting.GAMEFOLDER_PATH + register.getTitle();
		try {
			copyTransfer(register.getGamePath(), newFolderPath);
			System.out.println("Success!!!");
		} catch (IOException e) {
			System.err.println(e);
		}
		if (!register.getRegister()[3])
			// イメージファイルコピー
			try {
				String ets = getExtension(register.getImagePath());
				Files.createFile(FileSystems.getDefault().getPath(
						newFolderPath + "/$image" + ets));
				copyTransfer(register.getImagePath(), newFolderPath + "/$image"
						+ ets);
				System.out.println("Success!!!");
			} catch (IOException e) {
				System.err.println(e);
			}
	}

	/**
	 * ファイルの拡張子を得る
	 * 
	 * @param file
	 * @return
	 */
	private static String getExtension(String file) {
		int i = file.lastIndexOf(".");
		String ets = "";
		while (i < file.length()) {
			ets += file.charAt(i);
			i++;
		}
		return ets;
	}
	
	/**
	 * 
	 */
	private static void outputText() {
		if (!register.getRegister()[4]) {
			// 説明用ファイル
			String newTextPath = Setting.GAMEFOLDER_PATH + register.getTitle()
					+ "/$detail.txt";
			textWriter(newTextPath, register.getOutline());
		}
		if (!register.getRegister()[2]) {
			// ジャンルファイル
			String newGenrePath = Setting.GAMEFOLDER_PATH + register.getTitle()
					+ "/$genre.g";
			textWriter(newGenrePath, register.getGenre());
		}
	}

	/**
	 * 
	 */
	public static void textWriter(final String path, String... strings) {
		textWriter(FileSystems.getDefault().getPath(path), strings);
	}

	/**
	 * 新規で作成したいファイルのパスへ 書き込みたいstring型のテキスト配列を書き込む
	 * 
	 * @param newFilePath
	 * @param strings
	 */
	private static void textWriter(final Path newFilePath, String... strings) {
		try {
			Files.createFile(newFilePath);
			System.out.println("Created the new file.");
		} catch (IOException e) {
			System.out.println("Failed creating the new file.");
		}
		// ファイルにデータを書き込む
		try (BufferedWriter bw = Files.newBufferedWriter(newFilePath,
				StandardCharsets.UTF_8)) {
			for (int i = 0; i < strings.length; i++) {
				bw.write(strings[i]);
				if (i >= 1)
					bw.write("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * コピー元のパスから、コピー先のパスへ ファイルのコピーを行います。
	 *
	 * @param srcPath
	 *            :コピー元のパス
	 * @param destPath
	 *            :コピー先のパス
	 * @throws IOException
	 */
	public static void copyTransfer(final String srcPath, final String destPath)
			throws IOException {
		copyTransfer(new File(srcPath), new File(destPath));
	}

	/**
	 * コピー元のファイルから、コピー先のファイルへ ファイルのコピーを行います。
	 *
	 * @param src
	 *            :コピー元のパス
	 * @param dest
	 *            :コピー先のパス
	 * @throws IOException
	 */
	private static void copyTransfer(final File src, final File dest)
			throws IOException {
		if (src.isDirectory()) {
			// ディレクトリがない場合、作成
			if (!dest.exists()) {
				dest.mkdir();
			}
			String[] files = src.list();
			for (String file : files) {
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				copyTransfer(srcFile, destFile);
			}
		} else {
			// ファイルのコピー
			try (FileChannel srcChannel = new FileInputStream(src).getChannel();
					FileChannel destChannel = new FileOutputStream(dest)
							.getChannel();) {
				srcChannel.transferTo(0, srcChannel.size(), destChannel);
			}
		}
	}

	/*
	 * private static void copyTransfer() { }
	 */
	/**
	 * test ファイルが全てコピーできるかのテスト
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// AddGameFolder addGameFolder = new AddGameFolder();
		/*
		 * try {
		 * addGameFolder.copyTransfer("/Users/mayoineko/Downloads/Test",Setting
		 * .GAMEFOLDER_PATH+"114514"); System.out.println("完了");
		 * }catch(IOException e) { System.err.println(e); }
		 */
	}
}
