package main;

/**
 * Folderに登録したい情報を格納
 * @author mayoineko
 *
 */
public class Register {
	// ゲームのフォルダパス
	private String gamePath;
	// ゲームのタイトル
	private String gameTitle;
	// ゲームのジャンル
	private Genre[] genres;
	// ゲームのイメージファイルパス
	private String imagePath;
	// ゲームの説明ファイル
	private String outline;
	boolean[] error = new boolean[5];
	/**
	 * ジャンルの設定
	 * @param genres
	 */
	public void setGenres(Genre[] genres) {
		this.genres = genres;
	}
	/**
	 * ゲームフォルダパスの設定
	 * @param filePath
	 */
	public void setGamePath(String gamePath) {
		this.gamePath = gamePath;
	}
	/**
	 * ゲームタイトルの設定
	 * @param gameTitle
	 */
	public void setGameTitle(String gameTitle) {
		this.gameTitle = gameTitle;
	}
	/**
	 * 画像パスの設定
	 * @param imagePath
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	/**
	 * 説明用テキストの設定
	 * @param outline
	 */
	public void setOutline(String outline) {
		this.outline = outline;
	}
	/**
	 * 
	 */
	public void registerGameFolder() {
		AddGameFolder.addGame(this);
	}
	/**
	 * getter ゲームフォルダパス
	 * @return
	 */
	public String getGamePath() {
		return gamePath;
	}
	/**
	 * getter ゲームタイトル
	 * @return
	 */
	public String getTitle() {
		return gameTitle;
	}
	/**
	 *  getter ジャンル
	 * @return
	 */
	public String getGenre() {
		String g= "";
		for(Genre genre:genres)
			if(genre != null)
				g += genre.getLabel();
		return g;
	}
	/**
	 * getter 画像ファイルパス
	 * @return
	 */
	public String getImagePath() {
		return imagePath;
	}
	/**
	 * getter 説明用テキスト
	 * @return
	 */
	public String getOutline() {
		return outline;
	}
	/**
	 * フィールドが設定されているかの確認
	 * 
	 * @return
	 */
	public boolean[] setRegister() {
		for (int i=0;i<5;i++)
			error[i] = false;
		if(gamePath == null) {
			error[0] = true;
		}
		if(gameTitle.equals("")) {
			error[1] = true;
		}
		if(genres[0] == null && genres[1] == null && genres[2] == null) {
			error[2] = true;
		}
		if(imagePath == null) {
			error[3] = true;
		}
		if(outline.equals("")) {
			error[4] = true;
		}
		return error;
	}
	/**
	 * getter 登録できていないフィールド変数の番号
	 * @return
	 */
	public boolean[] getRegister() {
		return error;
	}
	/**
	 * 追加するゲームの情報を登録
	 */
	public final void addGameFolder() {
		registerGameFolder();
	}
}
