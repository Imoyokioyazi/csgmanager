package main;

/**
 * 設定クラス
 * @author mayoineko
 *
 */
public class Setting {
	private final static Configuration conf = Configuration.getConfiguration();
	/** Title */
	public final static String TITLE 
			= conf.getStringProperty("Title");
	/** stageの最小幅 */
	public final static double MIN_WIDTH = 1000;
	/** stageの最小高さ(TitleBar値22) */
	public final static double MIN_HEIGHT = 822;
	/** FullScreen設定 */
	public static boolean isFullScr = false;
	/** stageのresize設定 */
	public static boolean isResiza = true;
	/** GameFolderPath*/
	public final static String GAMEFOLDER_PATH 
			= conf.getStringProperty("GameFolder_Path");
	/** SettingFolderPath */
	public final static String SETTING_FOLDER_PATH 
			= conf.getStringProperty("SettingFolder_Path");
	/** Windowsのドライブ設定**/
	public final static String Drive
			= conf.getStringProperty("Drive");
}
