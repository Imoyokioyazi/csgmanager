package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * read a text file
 * 
 * @author mayoineko
 *
 */
public class TextReader {
	/**
	 * pathをもとにtextを読み込む
	 * textがなければnull
	 * 
	 * @param path
	 * @return
	 */
	public static String reads(String path) {
		String txt = "";
		File file = null;
		try{
			file = new File(path);
		} catch(Exception e) {
			System.err.println("File not Found");
		}
		if(file == null) return null;
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(file), "UTF-8"));
			String str = br.readLine();
			while(str!=null) {
				txt += "\n"+str;
				str = br.readLine();
			}
			br.close();
		} catch(IOException e) {
			System.err.println(e);
		}
		return txt;
	}
	public static String read(String path) throws IOException{
		String line ="";
		if(Files.notExists(FileSystems.getDefault().getPath(path)))
			System.err.println("File not Found");
		try(BufferedReader br = Files.newBufferedReader(Paths.get(path),StandardCharsets.UTF_8)) {
				line = br.readLine();
		}catch (IOException e) {
			System.err.println(e);
		}
		return line;
	}
	/**
	 * test用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			String text = TextReader.read("./GameFolder/個人製作シューティングゲーム/$genre.g");
			System.out.println(Integer.parseInt(text));
		} catch(IOException e) {
			
		}
	}
}